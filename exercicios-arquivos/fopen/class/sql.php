<?php 

// criando classe para comandos select, update e etc

	class sql extends PDO
	{
		private $conn;

		public function __construct(){

			$this->conn = new PDO("mysql:dbname=dbGabi;port=3306;host=192.168.14.224", "usr_teste", "1234");
		}

		
		private function setParams($statment, $parameters = array()){

			foreach($parameters as $key => $value){

			$this->setParam($statment, $key, $value);
			
			}

		}

		private function setParam($statment, $key, $value) {

			$statment->bindParam($key, $value);
		
		 }

		public function query($rawQuery, $params = array()){
			$stmt = $this->conn->prepare($rawQuery);

			$this->setParams($stmt, $params);

			$stmt->execute();

			return $stmt;
		}


		public function select($rawQuery, $params = array()):array
		{
			$stmt = $this->query($rawQuery, $params);

			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		
	}
?>